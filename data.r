## to do:
# make ramdomized inputs
setwd("~/Documents/Tutorium/r")


library(rvest)
library(quantmod)
library(TTR)
library(tidyverse)
library(tidyquant)

set.seed(10)

tbl <- read_html('https://en.wikipedia.org/wiki/List_of_S%26P_500_companies') %>% html_nodes(css = 'table')
tbl <- tbl[1] %>% html_table() %>% as.data.frame() %>% as_tibble()
relevant_stocks <- tbl %>% select(Symbol, Security, Date.first.added) %>% mutate(Date.first.added = parse_date(Date.first.added)) %>% 
  filter(Date.first.added < "2006-12-01") %>% filter(Symbol != "LUMN" & Symbol != "BF.B") %>% mutate(Security = Security %>% str_replace("&", "\\\\&"))


risk_free <- read.csv("t_bill.csv", stringsAsFactors = F)
risk_free <- risk_free[-(1:6), ]  %>% pull(2) %>% as.numeric()
risk_free <- risk_free/100 * 4 / 52.1429
risk_free <- as.data.frame(risk_free)


returns <- read.csv("returns.csv",  stringsAsFactors = F)
returns_names <- names(returns)
names(returns) <- substr(returns_names, 15,17)


excess_returns <- returns[,1:length(returns)] - risk_free[,1]

## look for assets with negative mean excess returns
mean_returns <- colMeans(excess_returns)
negative_mean_return <- names(mean_returns[mean_returns < 0])
negative_mean_return

for(i in negative_mean_return){
  relevant_stocks <- relevant_stocks %>% filter(Symbol != i)
}

returns_sp500 <- read_csv("return_market.csv")
excess_return_sp500 <- returns_sp500[,1] - risk_free[,1]
mean_excess_sp500 <- colMeans(excess_return_sp500)

mean_returns_smaller <- names(mean_returns[mean_returns > 2 * mean_excess_sp500])
for(i in mean_returns_smaller){
  relevant_stocks <- relevant_stocks %>% filter(Symbol != i)
}

AQR_funds <- c("AQR Diversified Arbitrage Fund","AQR Global Equity Fund", "AQR International Equity Fund", "AQR International Momentum Style Fund","AQR Large Cap Momentum Style Fund", "AQR Managed Futures Strategy Fund", "AQR Multi-Asset Fund",
               "AQR Small Cap Momentum Style Fund")

groups <- tibble(Group = 1, Security = relevant_stocks[sample(nrow(relevant_stocks), 6),] %>% pull(Security), Interest_rate = sample(2:15, 1), Risk_aversion = sample(seq(1, 10), 1), Fund = sample(AQR_funds, 1, replace = FALSE)) 

for (i in 2:80) {
  optimize_for_interest_rate <- sample(2:15, 1)
  risk_aversion <- sample(seq(1, 10), 1)
  fund <- sample(AQR_funds, 1, replace = FALSE)
  random_sample <- relevant_stocks[sample(nrow(relevant_stocks), 6,replace = TRUE),] %>% pull(Security) 
  
  groups <- groups %>% add_row(Group = i, Security = random_sample, Interest_rate = optimize_for_interest_rate, Risk_aversion = risk_aversion, Fund = fund)
}


# write_csv(groups, "final_groups.csv")







