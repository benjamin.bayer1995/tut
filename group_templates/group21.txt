\documentclass[letterpaper]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[top=1.2in, left=0.9in, bottom=1.2in, right=0.9in]{geometry} % sets the margins
\usepackage{amsmath,amsthm,amssymb}
\usepackage{url} % fixes url problem
\usepackage{csquotes}% Recommended
\usepackage[doublespacing]{setspace} % turns on double spacing
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=blue,
}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\rhead{Asset management assignment}
\lhead{Group 21 }
\rfoot{Page \thepage}





\title{Asset Management Winter Term 2020/2021 \\Excel Assignment Group 21 }
\date{}

\begin{document}
\maketitle

\section*{General Rules}

\begin{itemize}
\item Work in teams of two students. Do not forget to provide the names and student numbers of each team member on the first sheet of your document.
\item The assignment is due by January 31st, 2021 24:00.
\item Make sure that (only) one group member uploads the solution to Learn@WU. Be aware that the other group members might not be able to confirm the status.
\item Provide an Excel file with one sheet for the underlying downloaded series, one sheet for tasks 2 to 4 (named “statistics”) and one sheet (“optimization”) for tasks 5 to 8. Make sure that the tables are clearly marked and that the origin of every cell is transparent (you can use comments if in doubt).
\item Throughout this assignment, the optimization is to be performed with monthly figures directly.
\end{itemize}

\section*{Questions}

\begin{enumerate}
\item  For the following six US stocks (PNC Financial Services, Vulcan Materials, Target Corp., Equity Residential, Baxter International Inc. and Lincoln National) download the end-of-month historical adjusted share prices, dating back from the end of December 2006 to the end of December 2019 from \href{https://finance.yahoo.com}{Yahoo Finance} .

\item For each of the six stocks calculate the monthly returns in excess of the risk-free rate. Use the US Treasury Bill rate as risk-free rate. Choose the duration and date of the risk-free interest rate such that it is appropriate for the monthly stock investment period. Use only data from the official \href{https://www.federalreserve.gov/datadownload/Build.aspx?rel=H15&series=43249b7cd30524e4b5802a1cbf9db962&from=01/16/2002&to=10/17/2016&lastObs=&filetype=spreadsheetml&label=include&layout=seriescolumn}{Federal Reserve} website and document your choice and any transformation. 

\item Take the historical average monthly simple excess returns for each stock for the period from January 2007 to the end of December 2019 as the proxy for the expected excess rate of return. Note: In order to compute the gross return for January 2007, use the last adjusted closing price of December 2006 and the last adjusting closing price of January 2007. Only use the basic aritmetic operators (+, -, /, *) and name the vector of expected returns “ER”.

\item Calculate the variance-covariance matrix based on the historic monthly simple excess returns. Again, only use the basic aritmetic operators and name the monthly matrix “SIGMA”.

\item Without the help of the Excel plugin “Solver” identify the portfolio weights for the minimum variance portfolio. Only use Excel’s matrix formulas. What are the expected excess return and standard deviation of this portfolio?
  
\item There are two assumptions: a) the average risk-free rate over the period from January 2007 to December 2019 is the constant risk-free rate of the Markowitz model and b) you cannot invest into the risk-free asset. Solve for the efficient portfolio weights if the minimum expected gross portfolio return is 7\% p.a. using the Solver (provide a screenshot).

\item Calculate the tangency portfolio and state its expected excess rate of return together with its standard deviation. Use Excel’s matrix formulas to derive your result.

\item Assume quadratic utility function of the form $E[U(\tilde{r})]=E [\tilde{r}]- \frac{1}{2}A\sigma(\tilde{r})^2$  with a value of 7 for the parameter A. You are now allowed to invest in the risk-free asset at the constant rate as in (6.), what is the optimal portfolio choice?
  
  \item Obtain the following data:
  \begin{itemize}
\item monthly Fama-French 3 factors and the risk-free rate of return from \href{http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html}{Kenneth French’s data library website}

\item total return time series for the AQR Global Equity Fund Class R6 from the \href{https://funds.aqr.com/data-downloads}{AQR download site}

\item monthly adjusted close stock prices for Berkshire Hathaway Inc. (BRK-A) from \href{https://finance.yahoo.com/quote/BRK-A/history?p=BRK-A}{Yahoo Finance} 
\end{itemize}
In the following we will call the AQR Fund and Berkshire for short as \textit{portfolios}. If you need further data for tasks 10–13, use appropriate sources to get them.

\item Calculate the Beta, Jensen’s alpha and R2 for the two \textit{portfolios}. Present your results in an overview table and include a comment for every Jensen’s alpha estimate in which you note whether it is statistically significant at the 5\% level.

\item Calculate the Sharpe ratio, Treynor index, Modigliani \& Modigliani measure, tracking error, and information ratio for the two \textit{portfolios}. Use the market portfolio as benchmark if needed.

\item Regress the excess returns of the two \textit{portfolios} against the three Fama-French factors. Report the betas, alpha, and R2 values. Check if the betas and alphas are statistically significant at the 5\% level.

\item Comment on how did the alpha and R2 change compared to the CAPM regression.
\end{enumerate}

\end{document}


